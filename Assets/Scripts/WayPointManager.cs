﻿/// <summary>
/// Script Managers CamerType, CameraEffectsTypes, and Waypoint IDs.
/// Script Made By: Enoch Brewer
/// </summary>

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//
public enum CameraType {FREE_MOVEMNET, FORCED_LOOK, NONE}
public enum CameraEffectType {FADE_IN, FADE_OUT, SHAKE, SPLATTER}
public class WayPointManager : MonoBehaviour {
	
	public int currentWayPoint;
	public bool waiting;
	public Transform forcedLookObj;

	public static WayPointManager instance;

	void Awake() {
		if (instance == null) {
			instance = this;
		} else if (instance != this) {
			Destroy (this.gameObject);
		}
		currentWayPoint = 1;
	}
}
