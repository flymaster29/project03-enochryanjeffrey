﻿/// <summary>
/// his script allows for Look Return Movement
/// Script Made By: Enoch Brewer
/// </summary>

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.UI;

public class LookReturnMovement : WayPointBluePrint {
	public Transform startLocationTransform;
	public Transform lookLocationTransform;
	Vector3 startLocation;
	Vector3 lookLocation;
	//public float speed;
	bool shouldProceed;
	public float timeToFocus;
	public float timeToUnFocus;
	public float timeToWait;
	int entered = 0;

	//set default values, and get necessary data
	void Awake() {
		forcedLookScript = Camera.main.GetComponent<ForcedLookCameraFacing> ();
		freeMovementScript = Camera.main.GetComponent<FreeMovementCameraFacing> ();
		startLocationTransform.GetComponent<MeshRenderer> ().enabled = false;
		lookLocationTransform.GetComponent<MeshRenderer> ().enabled = false;
		startLocation = startLocationTransform.position;
		lookLocation = lookLocationTransform.position;
		shouldProceed = false;
	}
	// Use this for initialization
	void Start () {
		
	}

	void Update() {
		//The Logic for checking if preconditions for waypoint initiation have been met.
		if (player.position.x == startLocation.x && player.position.z == startLocation.z && entered < 1 && !WayPointManager.instance.waiting && WayPointManager.instance.currentWayPoint == waypointID) {
			entered++;
			if (cameraType == CameraType.FREE_MOVEMNET) {
				forcedLookScript.enabled = false;
				freeMovementScript.enabled = true;
			} else if (cameraType == CameraType.FORCED_LOOK) {
				freeMovementScript.enabled = false;
				forcedLookScript.enabled = true;

			} else if (cameraType == CameraType.NONE) {
				freeMovementScript.enabled = false;
				forcedLookScript.enabled = false;
			}
			if (cameraEffectType == CameraEffectType.FADE_IN) {
				Camera_Effects.instance.Fade ("in",fadeInTime,fadeImage);
			} else if (cameraEffectType == CameraEffectType.FADE_OUT) {
				Camera_Effects.instance.Fade ("out",fadeOutTime,fadeImage);
			} else if (cameraEffectType == CameraEffectType.SHAKE) {
				CameraShake.Shake (shakeTime, shakeIntensity);
			} else if (cameraEffectType == CameraEffectType.SPLATTER) {
				Camera_Effects.instance.Splatter (splatterTime,splatterImage);
			}
			shouldProceed = true;
		}
		//The Logic for checking if preconditions for waypoint initiation have been met.
		if (shouldProceed) {
			
			shouldProceed = false;
			StartCoroutine (DoMovement());

		}
	}
	IEnumerator DoMovement() {
		Vector3 targetDir = lookLocation - startLocation;
		Quaternion targetRot =  Quaternion.LookRotation (targetDir);
		Quaternion startRotation = player.rotation;

		var t = 0f;
		while (t < 1) {
			t += (Time.deltaTime / timeToFocus);
			player.rotation = Quaternion.Lerp (startRotation, targetRot, t);
			yield return null;
		}
		yield return new WaitForSeconds (timeToWait);

		t = 0f;
		while (t < 1) {
			t += (Time.deltaTime / timeToUnFocus);
			//player.rotation = Quaternion.Lerp (startRotation, targetRot, t);
			player.rotation = Quaternion.Lerp (targetRot, startRotation, t);
			yield return null;
		}
		WayPointManager.instance.currentWayPoint++;

	}
}

/// <summary>
/// Look return movement editor. This allows for editing the inspector with the information best suited.
/// </summary>
[CustomEditor(typeof(LookReturnMovement))]
public class LookReturnMovementEditor : Editor
{
	override public void OnInspectorGUI()
	{
		var myScript = target as LookReturnMovement;
		myScript.player = EditorGUILayout.ObjectField ("Player", myScript.player, typeof(Transform), true) as Transform;
		myScript.waypointID = EditorGUILayout.IntField ("Waypoint ID",myScript.waypointID);
		myScript.cameraType = (CameraType)EditorGUILayout.EnumPopup ("Camera Type", myScript.cameraType);
		if (myScript.cameraType == CameraType.FORCED_LOOK) {
			myScript.forcedLook = EditorGUILayout.ObjectField ("Forced Look", myScript.forcedLook, typeof(Transform), true) as Transform;
		}
		myScript.cameraEffectType = (CameraEffectType)EditorGUILayout.EnumPopup ("Camera Effect Type", myScript.cameraEffectType);
		if (myScript.cameraEffectType == CameraEffectType.FADE_IN) {
			EditorGUI.indentLevel++;
			myScript.fadeImage = EditorGUILayout.ObjectField ("Fade Image", myScript.fadeImage, typeof(Image), true) as Image;
			myScript.fadeInTime = EditorGUILayout.FloatField ("Fade In Time", myScript.fadeInTime);
			EditorGUI.indentLevel--;
		} else if (myScript.cameraEffectType == CameraEffectType.FADE_OUT) {
			EditorGUI.indentLevel++;
			myScript.fadeImage = EditorGUILayout.ObjectField ("Fade Image", myScript.fadeImage, typeof(Image), true) as Image;
			myScript.fadeOutTime = EditorGUILayout.FloatField ("Fade Out Time", myScript.fadeOutTime);
			EditorGUI.indentLevel--;
		} else if (myScript.cameraEffectType == CameraEffectType.SHAKE) {
			EditorGUI.indentLevel++;
			myScript.shakeTime = EditorGUILayout.FloatField ("Shake Time", myScript.shakeTime);
			myScript.shakeIntensity = EditorGUILayout.FloatField ("Shake Intensity", myScript.shakeIntensity);
			EditorGUI.indentLevel--;
		} else if (myScript.cameraEffectType == CameraEffectType.SPLATTER) {
			EditorGUI.indentLevel++;
			myScript.splatterImage = EditorGUILayout.ObjectField ("Splatter Image", myScript.splatterImage, typeof(Image), true) as Image;
			myScript.splatterTime = EditorGUILayout.FloatField ("Splatter Time", myScript.splatterTime);
			EditorGUI.indentLevel--;
		}
		myScript.startLocationTransform = EditorGUILayout.ObjectField ("Start Location", myScript.startLocationTransform, typeof(Transform), true) as Transform;
		myScript.lookLocationTransform = EditorGUILayout.ObjectField ("Look Location", myScript.lookLocationTransform, typeof(Transform), true) as Transform;
		myScript.timeToFocus = EditorGUILayout.FloatField ("Focus Time", myScript.timeToFocus);
		myScript.timeToUnFocus = EditorGUILayout.FloatField ("Unfocus Time", myScript.timeToUnFocus);
		myScript.timeToWait = EditorGUILayout.FloatField ("Wait Time", myScript.timeToWait);
	}
}