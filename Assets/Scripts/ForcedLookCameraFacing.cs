﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForcedLookCameraFacing : MonoBehaviour {

	public Quaternion original;
	public Transform lookAt;

	// Use this for initialization
	void Start () {
		
	}

	void OnEnable() {
		original = transform.rotation;
	}
	// Update is called once per frame
	void Update () {
		Camera.main.transform.LookAt (lookAt);
	}

	void OnDisable() {
		transform.rotation = original;
	}
}
