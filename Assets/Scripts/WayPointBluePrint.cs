﻿/// <summary>
/// Script Made By: Enoch Brewer
/// </summary>

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.UI;

/// <summary>
/// Way point blue print. Uses as the parent class for the waypoints, hold similar variabls amongst them
/// </summary>
public class WayPointBluePrint : MonoBehaviour {
	public Transform player;
	public int waypointID;
	public CameraType cameraType;
	public CameraEffectType cameraEffectType;
	public float fadeInTime;
	public float fadeOutTime;
	public Image fadeImage;
	public Image splatterImage;
	public float splatterTime;
	public float shakeTime;
	public float shakeIntensity;
	[HideInInspector]
	public ForcedLookCameraFacing forcedLookScript;
	[HideInInspector]
	public FreeMovementCameraFacing freeMovementScript;
	public Transform forcedLook;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}