﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Script_Effects : MonoBehaviour {

  bool isShaking = false;
  bool isSplattered = false;

  public float shakeDecay = 0.02f;
  public float shakeIntensity = 0.3f;

  public Image fade;
  public Image splatter;

  Vector3 originPos;
  Quaternion originRot;

  private void Awake() {
    splatter.canvasRenderer.SetAlpha(0f);
    Fade("in");

  }
  // Use this for initialization
  void Start() {

  }

  // Update is called once per frame
  void Update() {
    if(shakeIntensity > 0 && isShaking) {

      originPos = transform.position;
      originRot = transform.rotation;

      transform.position = originPos + Random.insideUnitSphere * shakeIntensity;
      transform.rotation = new Quaternion(originRot.x + Random.Range(-shakeIntensity, shakeIntensity) * .2f,
        originRot.y + Random.Range(-shakeIntensity, shakeIntensity) * .2f,
        originRot.z + Random.Range(-shakeIntensity, shakeIntensity) * .2f,
        originRot.w + Random.Range(-shakeIntensity, shakeIntensity) * .2f);

      shakeIntensity -= shakeDecay;
    }
    else if(isShaking) {
      isShaking = false;
    }

    if(isSplattered) {
      splatter.CrossFadeAlpha(0f, Time.deltaTime * 50, false);
    }
  }

  private void OnTriggerEnter(Collider other) {
    switch(other.gameObject.name) {
      case "Shake":
        Shake();
        break;
      case "Splatter":
        Splatter();
        break;
      case "Fade":
        Fade("out");
        break;
    }
  }

  void Shake() {
    if(shakeIntensity > 1.0f || shakeIntensity <= 0)
      shakeIntensity = 0.3f;
    if(shakeDecay > .1f || shakeDecay <= 0)
      shakeDecay = 0.02f;

    isShaking = true;
  }

  void Splatter() {
    if(!isSplattered) {
      splatter.canvasRenderer.SetAlpha(1f);
      splatter.CrossFadeAlpha(1f, Time.deltaTime, false);
      isSplattered = true;
    }
  }

  void Fade(string d) {
    switch(d) {
      case "in":
        fade.CrossFadeAlpha(0f, Time.deltaTime * 20, false);
        break;
      case "out":
        fade.CrossFadeAlpha(1f, Time.deltaTime * 20, false);
        break;
    }
  }
}
