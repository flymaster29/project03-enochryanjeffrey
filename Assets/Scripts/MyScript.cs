﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class MyScript : MonoBehaviour{
	public bool flag;
	public CameraType cameraType;// = CameraType.FORCED_LOOK;
	public Transform forcedLook;
	public int i = 1;
}
 
[CustomEditor(typeof(MyScript))]
public class MyScriptEditor : Editor {
	override public void OnInspectorGUI()
	{
		var myScript = target as MyScript;
		myScript.cameraType = (CameraType)EditorGUILayout.EnumPopup ("Camera Type", myScript.cameraType);
		if (myScript.cameraType == CameraType.FORCED_LOOK) {
			myScript.forcedLook = EditorGUILayout.ObjectField ("Forced Look", myScript.forcedLook, typeof(Transform), true) as Transform;
		}
	}
}