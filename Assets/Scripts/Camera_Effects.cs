﻿/// <summary>
/// This script handles the splatter, and fade logic
/// Script Made By: Ryan Mills
/// </summary>

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Camera_Effects: MonoBehaviour {
	bool isSplattered = false;

	public float shakeDecay = 0.02f;
	public float shakeIntensity = 0.3f;

	public GameObject fadeImageContainer;
	public GameObject splatterImageContainer;
	public Image fadeImage;
	public Image splatterImage;

	public float fadeInTime;
	public float fadeOutTime;

	Vector3 originPos;
	Quaternion originRot;


	public static Camera_Effects instance;

	void Awake() {
		isSplattered = false;
		//fadeImageContainer.SetActive (false);
		//splatterImageContainer.SetActive (false);
		if (instance == null) {
			instance = this;
		} else if (instance != this) {
			Destroy (this.gameObject);
		}
		splatterImage.canvasRenderer.SetAlpha (0f);
		fadeImage.canvasRenderer.SetAlpha (0f);
		//splatterImage.CrossFadeAlpha(0f, 0.1f, true);
	}

	// Use this for initialization
	void Start() {

	}

	// Update is called once per frame
	void Update() {
		if (isSplattered) {
			splatterImage.CrossFadeAlpha(0f, Time.deltaTime * 50, false);
		}
	}
	/// <summary>
	/// Splatter the specified time and splatterImg.
	/// </summary>
	/// <param name="time">Time.</param>
	/// <param name="splatterImg">Splatter image.</param>
	public void Splatter(float time, Image splatterImg) {
		if (!isSplattered) {
			splatterImageContainer.SetActive (true);
			splatterImage = splatterImg;
			splatterImage.canvasRenderer.SetAlpha(1f);
			splatterImage.CrossFadeAlpha(1f, time, false);
			isSplattered = true;
		}
	}
	/// <summary>
	/// Fade the specified d, time and fadeIMG.
	/// </summary>
	/// <param name="d">D.</param>
	/// <param name="time">Time.</param>
	/// <param name="fadeIMG">Fade IM.</param>

	public void Fade(string d, float time, Image fadeIMG) {
		fadeImage = fadeIMG;
		switch (d) {
		case "in":
			fadeImage.canvasRenderer.SetAlpha (1f);
			fadeImage.CrossFadeAlpha(0f, time, true);
			break;
		case "out":
			fadeImage.canvasRenderer.SetAlpha (0f);
			fadeImage.CrossFadeAlpha(1f, time, true);
			break;
		}
	}
}