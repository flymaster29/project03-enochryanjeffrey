﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FreeMovementCameraFacing : MonoBehaviour {

	public float speedH = 2f;
	public float speedV = 2f;

	private float yaw = 0.0f;
	private float pitch = 0f;

	// Use this for initialization
	void Start () {
		
	}
	// Update is called once per frame
	void Update () {
		FreeMovement ();
	}

	void FreeMovement() {
		yaw += speedH * Input.GetAxis ("Mouse X");
		pitch -= speedV * Input.GetAxis ("Mouse Y");
		Camera.main.transform.eulerAngles = new Vector3 (pitch, yaw, 0f);
	}
}
