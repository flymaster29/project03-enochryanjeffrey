/// <summary>
/// This script allows for Bezier Curve Movement
/// Script Made By: Enoch Brewer
/// </summary>

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.UI;

 
public class BezierCurveMovement : WayPointBluePrint {
	public Transform startLocationTransform;
	public Transform controlLocationTransform;
	public Transform endLocationTransform;
	Vector3 startLocation;
	Vector3 controlLocation;
	Vector3 endLocation;
	float CurveX;
	float CurveZ;
	float BezierTime;
	public float speed;

	bool shouldProceed;


	//set default values, and get necessary data
	void Awake() {
		forcedLookScript = Camera.main.GetComponent<ForcedLookCameraFacing> ();
		freeMovementScript = Camera.main.GetComponent<FreeMovementCameraFacing> ();
		startLocationTransform.GetComponent<MeshRenderer> ().enabled = false;
		controlLocationTransform.GetComponent<MeshRenderer> ().enabled = false;
		endLocationTransform.GetComponent<MeshRenderer> ().enabled = false;
		startLocation = startLocationTransform.position;
		controlLocation = controlLocationTransform.position;
		endLocation = endLocationTransform.position;
		shouldProceed = false;
	}


	void Update() {

		//The Logic for checking if preconditions for waypoint initiation have been met.
		if ( player.position.x == startLocation.x && player.position.z == startLocation.z && !WayPointManager.instance.waiting && WayPointManager.instance.currentWayPoint == waypointID) {
			if (cameraType == CameraType.FREE_MOVEMNET) {
				forcedLookScript.enabled = false;
				freeMovementScript.enabled = true;
			} else if (cameraType == CameraType.FORCED_LOOK) {
				Debug.Log ("Forced Look");
				freeMovementScript.enabled = false;
				forcedLookScript.enabled = true;
				forcedLookScript.lookAt = forcedLook;
				Debug.Log ("Forced Look?" + forcedLookScript.enabled);

			} else if (cameraType == CameraType.NONE) {
				freeMovementScript.enabled = false;
				forcedLookScript.enabled = false;
			}

			if (cameraEffectType == CameraEffectType.FADE_IN) {
				Camera_Effects.instance.Fade ("in",fadeInTime,fadeImage);
			} else if (cameraEffectType == CameraEffectType.FADE_OUT) {
				Camera_Effects.instance.Fade ("out",fadeOutTime,fadeImage);
			} else if (cameraEffectType == CameraEffectType.SHAKE) {
				CameraShake.Shake (shakeTime, shakeIntensity);
			} else if (cameraEffectType == CameraEffectType.SPLATTER) {
				Camera_Effects.instance.Splatter (splatterTime,splatterImage);
			}
			shouldProceed = true;
		}
		//The Logic for checking if preconditions for waypoint initiation have been met.
		if (shouldProceed) {
			
			//The math for the Bezier Curve
			if (Vector3.Distance (player.position, endLocation) > 1f) {
				BezierTime = BezierTime + Time.deltaTime * speed;
				if (BezierTime >= 1) {
					BezierTime = 0;
				}
				CurveX = (((1 - BezierTime) * (1 - BezierTime)) * startLocation.x) + (2 * BezierTime * (1 - BezierTime) * controlLocation.x) + ((BezierTime * BezierTime) * endLocation.x);
				CurveZ = (((1 - BezierTime) * (1 - BezierTime)) * startLocation.z) + (2 * BezierTime * (1 - BezierTime) * controlLocation.z) + ((BezierTime * BezierTime) * endLocation.z);
				player.position = new Vector3 (CurveX, 1, CurveZ);
			} else {
				//set location of character to the next end location
				player.position = new Vector3 (endLocation.x, 1, endLocation.z);
				shouldProceed = false;
				WayPointManager.instance.currentWayPoint++;
			}
		}
	}
}
/// <summary>
/// Bezier curve movement editor, This allows for editing the inspector with the information best suited.
/// </summary>
[CustomEditor(typeof(BezierCurveMovement))]
public class BezierCurveMovementEditor : Editor
{

	override public void OnInspectorGUI()
	{
		var myScript = target as BezierCurveMovement;
		myScript.player = EditorGUILayout.ObjectField ("Player", myScript.player, typeof(Transform), true) as Transform;
		myScript.waypointID = EditorGUILayout.IntField ("Waypoint ID",myScript.waypointID);
		myScript.cameraType = (CameraType)EditorGUILayout.EnumPopup ("Camera Type", myScript.cameraType);
		if (myScript.cameraType == CameraType.FORCED_LOOK) {
			EditorGUI.indentLevel++;
			myScript.forcedLook = EditorGUILayout.ObjectField ("Forced Look", myScript.forcedLook, typeof(Transform), true) as Transform;
			EditorGUI.indentLevel--;
		}
		myScript.cameraEffectType = (CameraEffectType)EditorGUILayout.EnumPopup ("Camera Effect Type", myScript.cameraEffectType);
		if (myScript.cameraEffectType == CameraEffectType.FADE_IN) {
			EditorGUI.indentLevel++;
			myScript.fadeImage = EditorGUILayout.ObjectField ("Fade Image", myScript.fadeImage, typeof(Image), true) as Image;
			myScript.fadeInTime = EditorGUILayout.FloatField ("Fade In Time", myScript.fadeInTime);
			EditorGUI.indentLevel--;
		} else if (myScript.cameraEffectType == CameraEffectType.FADE_OUT) {
			EditorGUI.indentLevel++;
			myScript.fadeImage = EditorGUILayout.ObjectField ("Fade Image", myScript.fadeImage, typeof(Image), true) as Image;
			myScript.fadeOutTime = EditorGUILayout.FloatField ("Fade Out Time", myScript.fadeOutTime);
			EditorGUI.indentLevel--;
		} else if (myScript.cameraEffectType == CameraEffectType.SHAKE) {
			EditorGUI.indentLevel++;
			myScript.shakeTime = EditorGUILayout.FloatField ("Shake Time", myScript.shakeTime);
			myScript.shakeIntensity = EditorGUILayout.FloatField ("Shake Intensity", myScript.shakeIntensity);
			EditorGUI.indentLevel--;
		} else if (myScript.cameraEffectType == CameraEffectType.SPLATTER) {
			EditorGUI.indentLevel++;
			myScript.splatterImage = EditorGUILayout.ObjectField ("Splatter Image", myScript.splatterImage, typeof(Image), true) as Image;
			myScript.splatterTime = EditorGUILayout.FloatField ("Splatter Time", myScript.splatterTime);
			EditorGUI.indentLevel--;
		}
		myScript.startLocationTransform = EditorGUILayout.ObjectField ("Start Location", myScript.startLocationTransform, typeof(Transform), true) as Transform;
		myScript.controlLocationTransform = EditorGUILayout.ObjectField ("Control Location", myScript.controlLocationTransform, typeof(Transform), true) as Transform;
		myScript.endLocationTransform = EditorGUILayout.ObjectField ("End Location", myScript.endLocationTransform, typeof(Transform), true) as Transform;
		myScript.speed = EditorGUILayout.FloatField ("Speed", myScript.speed);
	}
}
